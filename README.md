# demo-seminario-estg

## Simple setup guide
1. Clone the repository
```shell
git clone https://gitlab.com/diogoramos/demo-seminario-estg.git
cd demo-seminario-estg
```
2. Configure `.env` file for your choice or leave it as default. You can put there anything you like, it will be used to configure you services

3. Make sure you have python installed

4. Start Docker

5. Run the Infrastructure by this one line:
```shell
./setup.sh
```
This command will build and run containers for:
* Zeppelin available on http://localhost:8080
* S3 available on http://localhost:9000
* MySql
* MLFlow available on http://localhost:5000 (Tracking Server)

The command will also load an example notebook to zeppelin called `demo-seminario-estg` and autommatically create an S3 bucket to store model artifacts.

