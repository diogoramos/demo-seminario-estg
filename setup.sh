#!/bin/bash

source .env

echo "# MLFLOW CONFIG" >> ~/.bashrc
echo "export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID" >> ~/.bashrc
echo "export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY" >> ~/.bashrc
echo "export MLFLOW_S3_ENDPOINT_URL=$MLFLOW_S3_ENDPOINT_URL" >> ~/.bashrc
echo "export MLFLOW_TRACKING_URI=$MLFLOW_TRACKING_URI" >> ~/.bashrc
echo "# END MLFLOW CONFIG" >> ~/.bashrc

echo "[ OK ] Successfully installed environment variables into your .bashrc!"

echo "Building and Starting the Services"
docker-compose up -d

echo "Create S3 Bucket"
bash ./setup/run_create_bucket.sh

echo "Giving permissions to user zeppelin"
docker exec -u 0 -it zeppelin chown -R zeppelin /opt/conda/lib/python3.7/site-packages/ /opt/conda/bin/

echo "Loading examples on Zeppelin"
docker exec -u 0 -it zeppelin curl -s -XPOST http://localhost:8080/api/notebook/import -d @'/data/demo-seminario-estg.zpln'