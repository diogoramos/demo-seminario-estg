#!/bin/bash

set -o allexport; source .env; set +o allexport

pip3 install --upgrade pip
pip3 install minio
python3 ./setup/create_bucket.py
